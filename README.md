```
╔╦╗┌─┐┌─┐┌─┐╔═╗┬  ┬┌┬┐┌─┐┬ ┬┌─┐╔╦╗┬─┐┌─┐┌┐┌
║║║├┤ │ ┬├─┤║ ╦│  │ │ │  ├─┤├─┤ ║ ├┬┘│ ││││
╩ ╩└─┘└─┘┴ ┴╚═╝┴─┘┴ ┴ └─┘┴ ┴┴ ┴ ╩ ┴└─└─┘┘└┘                                                      
```
![megaglitchatron](https://v3d.space/img/megaglitchatron.gif)

MegaGlitchaTron - A Command Line Glitch Art Script

For any questions / feature requests / bugs please open an issue here: https://gitlab.com/hacklab01/MegaGlitchATron/-/issues .

Software needed:

- Git Bash for Windows https://git-scm.com/downloads (includes the bash shell and software such as sed – a stream editor)
- MacOS ships with everything installed
- Linux should use bash shell and install sed (included in most distros)
- FFmpeg is used for preparing input material, stabilizing output, video / image edits
- FFmpeg install instructions for MacOS and Windows: https://formatc.hr/datamosh101/
- to use FFmpeg on Linux install it via package manager
- for gif mode install imagemagick https://imagemagick.org

When everything is installed:
- grab code (clone or just download): https://gitlab.com/hacklab01/MegaGlitchATron/
- put your files (video or image) in the same directory where you put the script and run it. 

Example: 

Take a file called input.mp4, apply a sonification effect to it and save output to a file called output.mp4 (actual output file name may differ).

```
bash mgt.sh -i input.mp4 -m son -o output.mp4
```

```
Usage: mgt.sh [-b] -i input_file [-m mode] -o output_file

Options:
    b - Enable batch mode - works on a directory. Place source files in a directory called 'batch' in the working directory.
    h - Displays this help.
    i - Set input filename. (Ignored in batch mode).
    m - Set mode: hex, son, gif, mosh or test. (Ignored in batch mode).
    o - Set output file name. (Ignored in batch mode, or mosh mode).
    U - Upgrade script with the latest version from Gitlab. (May cause bugs).

Modes:
    hex: Uses sed to search and replace a string.
    son: Uses ffmpeg to do sonification (treats video / images as audio).
    gif: Creates a glitched gif out of a static image.
        PBM format - black and white output
        PGM format - greyscale output
        PAM format - color output
    mosh: Uses tomato.py script for datamosh (https://github.com/itsKaspar/tomato)
    test: Converts the input to all codecs supported by current ffmpeg version and runs a batch operation on all created files.
```
