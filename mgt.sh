#!/bin/bash

# Copyleft v3d/crash-stop@hacklab01/fubar.space/formatc.hr, use at your own peril.
# For newest (buggy) version goto https://gitlab.com/hacklab01/MegaGlitchATron

#pretty colors
black=$(tput setaf 0)
red=$(tput setaf 1)
green=$(tput setaf 2)
yellow=$(tput setaf 3)
blue=$(tput setaf 4)  
magenta=$(tput setaf 5)
cyan=$(tput setaf 6)   
white=$(tput setaf 7)  

# check whitch OS we're using and set the imagemagick command accordingly
# from https://stackoverflow.com/a/8597411

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  # Linux
  imagick_command=""
elif [[ "$OSTYPE" == "darwin"* ]]; then
  # Mac OSX
  imagick_command=""
elif [[ "$OSTYPE" == "cygwin" ]]; then
  # POSIX compatibility layer and Linux environment emulation for Windows
  imagick_command=""
elif [[ "$OSTYPE" == "msys" ]]; then
  # Lightweight shell and GNU utilities compiled for Windows (part of MinGW)
  imagick_command="magick"
elif [[ "$OSTYPE" == "win32" ]]; then
  # 32 bit windows - untested
  imagick_command="magick"
elif [[ "$OSTYPE" == "freebsd"* ]]; then
  # freeBSD
  imagick_command=""
else
  # Unknown OS
  echo "Unknown OS, go into the script and edit the imagick_command variable manually (can be magick or convert)"
fi

#upgrade (if -U is provided)

# URL of the raw file
file_url="https://gitlab.com/hacklab01/MegaGlitchATron/-/raw/main/mgt.sh"

# Function to download the file
upgrade_mgt() {
    curl -s "$file_url" -o "$0"  # Overwrite the script file with the downloaded content
    chmod +x "$0"  # Make the script executable again
}

# Check if the "-U" parameter is provided along with additional parameters
if [ "$1" == "-U" ] && [ $# -gt 1 ]; then
    echo "${red}Invalid usage. When using '-U', no additional parameters are allowed."
    exit 1
fi

# Check if only the "-U" parameter is provided
if [ "$1" == "-U" ]; then
    # Fetch the file and extract the CRC from the response headers
    response_headers=$(curl -sI "$file_url")
    crc=$(echo "$response_headers" | grep -iE "^ETag: [a-z0-9]+" | awk '{print $2}' | tr -d '"')

    # Get the current CRC of the script
    current_crc=$(git rev-parse HEAD)

    # Compare the CRCs
    if [ "$crc" != "$current_crc" ]; then
        echo "Updating the script..."
        upgrade_mgt
        echo "Mgt updated successfully."
        exit 0
    else
        echo "Mgt is up to date."
        exit 0
    fi
fi

#banner function

hello_mgt() {

echo "${red}╔╦╗┌─┐┌─┐┌─┐╔═╗┬  ┬┌┬┐┌─┐┬ ┬┌─┐╔╦╗┬─┐┌─┐┌┐┌"
echo "${green}║║║├┤ │ ┬├─┤║ ╦│  │ │ │  ├─┤├─┤ ║ ├┬┘│ ││││"
echo "${blue}╩ ╩└─┘└─┘┴ ┴╚═╝┴─┘┴ ┴ └─┘┴ ┴┴ ┴ ╩ ┴└─└─┘┘└┘"
}

cursor_animation() {
    local cursor="/-\\|"
    local count=0

    # Start the animation loop
    while :
    do
        # Print the current cursor character
        printf "\r${cursor:$count:1}"

        # Increment the count
        count=$(( (count + 1) % 4 ))

        # Sleep for a short duration to control the animation speed
        sleep 0.2
    done
}

stop_cursor_animation() {
    # Terminate the animation loop
    kill $cursor_animation_pid >/dev/null 2>&1
}

cleanup() {
    stop_cursor_animation
    exit 0
}

# Make a help function we can reuse anywhere
mgt_help()
{
   # Display help
   __help="
${red}Mega${green}Glitcha${blue}Tron - ${yellow}A command line glitch art script

${red}Usage: mgt.sh [-b] -i input_file [-m mode] -o output_file

${green}Options:
    ${white}i - Set input file name.
    b - Enable batch mode - works on a directory. Place source files in a directory called 'batch' in the working directory.
    h - Displays this help.
    i - Set input filename. (Ignored in batch mode).
    m - Set mode: hex, son, gif, mosh or test. (Ignored in batch mode).
    o - Set output file name. (Ignored in batch mode, or mosh mode).
    U - Upgrade script with the latest version from Gitlab. ${red}(May cause bugs).

${green}Modes:
    ${white}hex: Uses sed to search and replace a string.
    son: Uses ffmpeg to do sonification (treats video / images as audio).
    gif: Creates a glitched gif out of a static image.
        PBM format - black and white output
        PGM format - greyscale output
        PAM format - color output
    mosh: Uses tomato.py script for datamosh (https://github.com/itsKaspar/tomato)
    test: Converts the input to all codecs supported by current ffmpeg version and runs a batch operation on all created files.
"
   echo "$__help"
}

# Cleanup function

ask_cleanup() {
    while true; do
      read -p "${red}Clean up after (keep only output)? Y(es) / N(o) [Y]: " yn
      case $yn in
        [yY] | [yY][eE][sS] | "" )  # Accepts 'y', 'Y', 'yes', 'YES', or empty input
          clean="true"
          break
          ;;
        [nN] | [nN][oO] )  # Accepts 'n', 'N', 'no', 'NO'
          clean="false"
          break
          ;;
        * )
          echo "${red}Invalid response"
          ;;
      esac
    done
}

#TODO: not working, filenames need to be adjusted to fit all modes

transplant_audio() {
      read -p "${green}Transplant source file audio onto glitched and baked video? (Y/N, default: N): " transplant_audio
    transplant_audio=${transplant_audio:-N}
    if [ "$transplant_audio" == "Y" ] || [ "$transplant_audio" == "y" ]; then
        ffmpeg -v quiet -stats -i "$output" -i "$input" -c copy -map 0:v:0 -map 1:a:0 -shortest "$output_with_sound.mp4" -y
    fi
}

hex_mode() {
    now="$(date +'%Y%m%d-%H%M%S')"
    read -e -r -p "${green}Enter hex pattern for replacement (default: s/\xDA/\x0E\xDA/g, 'random' for random hex): " -i "s/\xDA/\x0E\xDA/g" hex 
    if [ "$hex" == "random" ]; then
        hex="s/\\x$(openssl rand -hex 1)/\\x$(openssl rand -hex 1)\\x$(openssl rand -hex 1)/g"
    else
        hex="${hex:-\xDA/\x0E\xDA}"
    fi
    if [ "$batch" = true ]
    then
        echo "${green}Batch mode selected"
    else
        mkdir "$now-$input-hex-frames"
    fi
    if [ "$batch" = true ]
    then
        mkdir ./batch/done/
        echo "${magenta}Performing hex replacement $hex${white}"
        cursor_animation &
        cursor_animation_pid=$!
        for i in ./batch/*.*; do
            sed "$hex" "$i" > temp
            mv temp "$i"
        done
        stop_cursor_animation
    else
        echo "${magenta}Dumping video frames to $now-$input-hex-frames directory." 
        echo "${blue}Command used: ${yellow}ffmpeg -v quiet -stats -i $input $now-$input-hex-frames/%05d.bmp ${white}"
        ffmpeg -v quiet -stats -i $input $now-$input-hex-frames/%05d.bmp
        echo "${magenta}Performing hex replacement $hex${white}"
        cursor_animation &
        cursor_animation_pid=$!
        for i in "$now-$input-hex-frames"/*.bmp; do
            sed "$hex" "$i" > temp
            mv temp "$i"
        done
        stop_cursor_animation
        echo "${magenta}Encoding hexed frames to video." 
        echo "${blue}Command used: ${yellow}ffmpeg -v quiet -stats -framerate 25 -i "$now-$input-hex-frames/%05d.bmp" -pixel_format yuv420p -c:v h264 -q:v 0 "$output" -y${white}"
        ffmpeg -v quiet -stats -framerate 25 -i $now-$input-hex-frames/%05d.bmp -pixel_format yuv420p -c:v h264 -q:v 0 $output-$mode-$bake.mp4 -y
    fi
    #not working yet: transplant_audio
    # Clean up
    if [ "$clean" == true ]; then
        echo "Cleaning up..."
        rm "$now-$input-hex-frames"/*
        rmdir "$now-$input-hex-frames"
        rm temp
    else
        exit 0
    fi

    # We're done, so exit cleanly
    exit 0
}


son_mode() {
      if [ "$batch" = true ]
    then
      # echo "${white}Running sonification on $input..."
      # Effect options for sonification
      effect_options=("crusher" "phaser" "chorus" "eesser")
      # Prompt the user to select the effect
      # Check https://www.vacing.com/ffmpeg_audio_filters/index.html 
      # or https://www.ffmpeg.org/ffmpeg-filters.html for extra effects

      echo "${green}Select an effect for sonification:"
      select fx in "${effect_options[@]}"
      do
        case $fx in
          "crusher")
            effect="acrusher=level_in=18:level_out=2:bits=1:mode=log:aa=1,adelay=1500"
            break
            ;;
          "phaser")
            effect="aphaser=type=t:speed=1:decay=0.1"
            break
            ;;
          "chorus")
            effect="chorus=0.5:0.9:50|60|70:0.3|0.22|0.3:0.25|0.4|0.3:2|2.3|1.3"
            break
            ;;
          "eesser")
            effect="deesser=i=1:s=e[a];[a]aeval=val(ch)*10:c=same"
            break
            ;;
          "custom")
            read -p "Enter your custom effect string: " custom_effect
            effect="$custom_effect"
            break
            ;;
          *)
            echo "${red}Invalid option. Please try again."
            ;;
        esac

      done
      # Check video resolution and set filename
      for input in ./batch/*.*
        do video_size=$(ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0 "$input")
           output="$input-$video_size-$fx"
           # Convert input video to raw.
           echo "${magenta}Converting video to raw"
           echo "${blue}Command used: ${yellow}ffmpeg -i "$input" -c:v rawvideo -pixel_format yuv420p "$output-raw-prep.yuv" -y${white}"
           ffmpeg -v quiet -stats -i "$input" -c:v rawvideo -pixel_format yuv420p "$output-raw-prep.yuv" -y
           # Apply sonification effect
           echo "${magenta}Applying sonification effect"
           echo "${blue}Command used: ${yellow}ffmpeg -f alaw -i "$output-raw-prep.yuv" -af "$effect" -f alaw "$output-sonified-output.yuv" -y${white}"
           ffmpeg -v quiet -stats -f alaw -i "$output-raw-prep.yuv" -af "$effect" -f alaw "$output-sonified-output.yuv" -y
           # Convert the sonified raw back into video.
           echo "${magenta}Converting the sonified raw back into video"
           echo "${blue}Command used: ${yellow}ffmpeg -v quiet -stats -f rawvideo -framerate 30 -pixel_format yuv420p -video_size "$video_size" -i "$output-sonified-output.yuv" -c:v h264 -q:v 0 "$output-sonified-bake.mp4" -y${white}"
           ffmpeg -v quiet -stats -f rawvideo -framerate 30 -pixel_format yuv420p -video_size "$video_size" -i "$output-sonified-output.yuv" -c:v h264 -q:v 0 "$output-$mode-bake.mp4" -y
           # Convert the baked mp4 file into a PNG
           extension="${input##*.}"
           # List of common image extensions
           image_extensions=("jpg" "jpeg" "png" "gif" "bmp")
           is_in_list() {
            local target="$1"
            shift
            for item in "$@"; do
              [[ "$target" == "$item" ]] && return 0
            done
            return 1
          }
          if is_in_list "$extension" "${image_extensions[@]}"; then
            ffmpeg -i $output-$mode-bake.mp4 $output-$mode-bake.png
            rm $output-$mode-bake.mp4
            # Add your action for image files here
          else
          echo
          fi           
           # Todo: transplant source file audio onto glitched and baked video.

        done
      else

  echo "${white}Running sonification on $input..."
  # Effect options for sonification
  effect_options=("crusher" "phaser" "chorus" "eesser")
  # Prompt the user to select the effect
  # Check https://www.vacing.com/ffmpeg_audio_filters/index.html 
  # or https://www.ffmpeg.org/ffmpeg-filters.html for extra effects

  echo "${green}Select an effect for sonification:"
  select fx in "${effect_options[@]}"
  do
    case $fx in
      "crusher")
        effect="acrusher=level_in=18:level_out=2:bits=1:mode=log:aa=1,adelay=1500"
        break
        ;;
      "phaser")
        effect="aphaser=type=t:speed=1:decay=0.1"
        break
        ;;
      "chorus")
        effect="chorus=0.5:0.9:50|60|70:0.3|0.22|0.3:0.25|0.4|0.3:2|2.3|1.3"
        break
        ;;
      "eesser")
        effect="deesser=i=1:s=e[a];[a]aeval=val(ch)*10:c=same"
        break
        ;;
      "custom")
        read -p "Enter your custom effect string: " custom_effect
        effect="$custom_effect"
        break
        ;;
      *)
        echo "${red}Invalid option. Please try again."
        ;;
    esac

  done
  # Check video resolution and set filename
  video_size=$(ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0 "$input")
  output="$output-$video_size-$fx"

  # Convert input video to raw.
  echo "${magenta}Converting video to raw"
  echo "${blue}Command used: ${yellow}ffmpeg -i "$input" -c:v rawvideo -pixel_format yuv420p "$output-raw-prep.yuv" -y${white}"
  ffmpeg -v quiet -stats -i "$input" -c:v rawvideo -pixel_format yuv420p "$output-raw-prep.yuv" -y

  # Apply sonification effect
  echo "${magenta}Applying sonification effect"
  echo "${blue}Command used: ${yellow}ffmpeg -f alaw -i "$output-raw-prep.yuv" -af "$effect" -f alaw "$output-sonified-output.yuv" -y${white}"
  ffmpeg -v quiet -stats -f alaw -i "$output-raw-prep.yuv" -af "$effect" -f alaw "$output-sonified-output.yuv" -y

  # Convert the sonified raw back into video.
  echo "${magenta}Converting the sonified raw back into video"
  echo "${blue}Command used: ${yellow}ffmpeg -v quiet -stats -f rawvideo -framerate 30 -pixel_format yuv420p -video_size "$video_size" -i "$output-sonified-output.yuv" -c:v h264 -q:v 0 "$output-sonified-bake.mp4" -y${white}"
  ffmpeg -v quiet -stats -f rawvideo -framerate 30 -pixel_format yuv420p -video_size "$video_size" -i "$output-sonified-output.yuv" -c:v h264 -q:v 0 "$output-$mode-bake.mp4" -y

  # Todo: transplant source file audio onto glitched and baked video.
  fi
  # Remove any yuv files we created (they tend to be huge!)
  if [ "$clean" == true ]; then
    echo "Cleaning up..."
    # Remove everything the script made
    rm *.yuv
    rm ./batch/*.yuv
  else
    # Or exit
    exit 0
  fi

  exit 0

}

gif_mode() {
  now="$(date +'%Y%m%d-%H%M%S')"
  # Pick a format to use for making the frames
  select pg in "PBM format - black and white output" "PGM format - greyscale output" "PPM format - color output"
  do
    case $pg in 
      "PBM format - black and white output")
        format="pbm"
        break;;
      "PGM format - greyscale output")
        format="pgm"
        break;;
      "PPM format - color output")
        format="ppm"
        break;;
    esac
  done
  echo -n "How many iterations (whole number) : " 
  # Read variable n from user input.
  read n
  # Set variable i to 0 (i will hold the number of iterations until it reaches n)
  i=0
  # 
  # Resize info here https://imagemagick.org/Usage/resize/#resize
  # Make a directory that will hold the frames
  mkdir $now-$input-frames
  # We set $imagick_command earlier, depending on our OS
  $imagick_command convert -resize 640x480\! $input test.$format
  # Run while loop while $i is less then (-lt) $n (run help test in your terminal to see all supported test operators in your shell)
  while [ $i -lt $n ]
    do 
      # Increment $i by 1 for each loop as long as the condition is true
      ((i++))
      # Use sed to transfer the header of resized $input file
  #   sed '2,$d' test.$format > head.$format;
      # Strip header ( to avoid damaging it)
  #   sed '1d'  test.$format > swap.$format
      # Generate random numbers
      # Generate no from 1 to 64
      seed1=$(( $RANDOM % 32 + 1 ))
      seed2=$(( $RANDOM % 64 + 1 ))
      seed3=$(( $RANDOM % 32 + 1 ))
      seed4=$(( $RANDOM % 64 + 1 ))
      # Convert to hex
      from=$(printf '%x\n' $seed1)
      to=$(printf '%x\n' $seed2)
      from2=$(printf '%x\n' $seed3)
      to2=$(printf '%x\n' $seed4)      
      #xxd -p swap.$format | sed 's/'$from''$from2'/'$to''$to2'/g' | xxd -p -r > help.$format
      sed -b '5,$s/'"$from"/"$to""$to2"'/g' test.$format > $now-$input-frames/$i.$format
      # Put the header back
  #   cat head.$format help.$format > headswap.$format;
      # Convert headswap.$format image-$i.ppm 
  #    $imagick_command convert headswap.$format $now-$input-frames/$i.ppm
      # Clean up
  #    rm help.$format
      # Finish our while loop
  done
  # Morphgif
  # Save input
  mv $input saved_input
  # Convert frames to gif
  $imagick_command convert -delay 5 $now-$input-frames/*.$format -morph 14 gifatron-$from-$from2-$to-$to2-$output.gif
  mv saved_input $input
  # Clean up
          # If user chose yes to clean (clean is set to true, mind the quotes!)
  if [ "$clean" == true ]
  then
    echo "Cleaning up..."
    # Remove everything the script made
    rm -rf $now-$input-frames/
    rm *.ppm
    rm *.$format        
  else 
    # Or exit (if $clean is false)
    exit 0
  fi
  exit 0
}

mosh_mode() {
  file_path="./tomato.py"
  url="https://raw.githubusercontent.com/itsKaspar/tomato/master/tomato.py"
  required_python_version="3"

  # Check if Python version is 3 or higher
  python_version=$(python -c 'import platform; print(platform.python_version())' 2>/dev/null)

  if [ -z "$python_version" ]; then
    echo "${red}Error: Python 3 is required but not found. Please install Python 3."
    exit 1
  fi

  # Remove leading 'v' from Python version if present
  python_version=${python_version#v}

  if [ "$(printf '%s\n' "$required_python_version" "$python_version" | sort -V | head -n1)" != "$required_python_version" ]; then
    echo "${red}Error: Python version $required_python_version or higher is required, but found version $python_version."
    exit 1
  fi

  if [ -e "$file_path" ]; then
    echo "${green}tomato.py already exists in the current directory. ✓"
  else
    echo "${green}Downloading tomato.py..."
    if command -v curl >/dev/null 2>&1; then
      curl -o "$file_path" "$url"
    elif command -v wget >/dev/null 2>&1; then
      wget -O "$file_path" "$url"
    else
      echo "${red}Error: Neither curl nor wget is installed. Unable to download the file."
      exit 1
    fi

    if [ -e "$file_path" ]; then
      echo "${green}tomato.py downloaded successfully. ✓"
    else
      echo "${red}Error: Failed to download tomato.py."
      exit 1
    fi
  fi
  effect=""
  mosh_prep="${input%.*}"
  tomato_options=("Take out iframes" "Duplicate the 100th frame 50 times" "Duplicate a frame 5 times every 10 frames" "Shuffle all of the frames in the video" "Custom" "Display tomato help")

  echo "${green}Select tomato options:"
  select tom in "${tomato_options[@]}"
  do
    case $tom in
      "Take out iframes")
        break
        ;;
      "Duplicate the 100th frame 50 times")
        m="bloom"
        c="50"
        n="100"
        break
        ;;
      "Duplicate a frame 5 times every 10 frames")
        m="pulse"
        c="5"
        n="10"
        break
        ;;
      "Shuffle all of the frames in the video")
        m="random"
        break
        ;;
      "Custom")
        read -e -r -p "${yellow}Enter mode (-m) (options: pulse, bloom): " m
        read -e -r -p "${yellow}Effect timing (-c):" c
        read -e -r -p "${yellow}Target frame(-n):" n
        break
        ;;
      "Display tomato help")
        python tomato.py -h
        break
        ;;
      *)
        echo "${red}Invalid option. Please try again."
        ;;
    esac
  done
  
  #check if extension is avi and prep the file if it isn't

  extension="${input##*.}" 
  if [ -n "$batch" ] && [ "$extension" != "avi" ]; then
    echo "${red}Input file is not an .avi file, prepping for mosh.${white}"
    ffmpeg -v quiet -stats -i $input -q:v 0 -an -bf 0 -g 9999 -c:v mpeg4 $mosh_prep.avi
    input=$mosh_prep.avi
  else
    echo "${red}Input file is an .avi file, proceeding with mosh.${white}"
  fi
  if [ "$batch" = true ]; then
    echo "${magenta}Performing datamosh with tomato.py"
    for i in ./batch/*.avi; do
      mosh_prep="${i%.*}"
      echo $mosh_prep.avi
      #echo "tomato -i $mosh_prep.avi -m $m -c $c -n $n"
      if [[ -z "$m" ]]; then
      #python tomato.py -i "$i" -n "$n" -c "$c"
      python tomato.py -i "$i"
      elif [[ "$m" == "random" ]]; then
            python tomato.py -m "$m" -i "$i"
      else 
            python tomato.py -m "$m" -i "$i" -n "$n" -c "$c"
      fi 
    done
  else
      if [[ -z "$m" ]]; then
            echo "${magenta}Using command: ${yellow}python tomato.py -i $input"
            python tomato.py -i "$input"
      elif [[ "$m" == "random" ]]; then
            echo "${magenta}Using command: ${yellow}python tomato.py -m $m -i $input"
            python tomato.py -m "$m" -i "$input"
      else 
            echo "${magenta}Using command: ${yellow} python tomato.py -m $m -i $input -n $n -c $c"
            python tomato.py -m "$m" -i "$input" -n "$n" -c "$c"
      fi 
  fi
}
 
test_mode() {
  output="${input%.*}"
  #get list of encoders 
  ffmpeg -v quiet -stats -encoders | awk '/^[[:space:]]+[V]/ && !/subtitle|audio/ { print $2 }' | sed -e '/^=/d' > encoders
  #make a subdir
  mkdir batch
  #run an ffmpeg command for each codec in the list 
  while IFS="" read -r p || [ -n "$p" ]; do
    if [ -n "$p" ]; then
        echo "${magenta}Encoding $output-$p.avi${white}. Using ffmpeg -v quiet -stats -i $input -c:v $p -q:v 0 -an -bf 0 -g 99999 -an batch/$output-$p.avi"
        ffmpeg -v quiet -stats -i "$input" -c:v "$p" -q:v 0 -an -bf 0 -g 99999 -an "batch/$output-$p.avi"
    fi
  done < encoders 
  if [ "$clean" == true ]
  then
    echo "Cleaning up..."
    # Remove everything the script made
    rm encoders        
  else 
    # Or exit (if $clean is false)
    exit 0
  fi
  exit 0
}

# Set the signal handler for termination signals
trap cleanup EXIT

#Vars
batch=false
input=""
mode=""
output=""

#Banner
hello_mgt

# Parse command line arguments
while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
        -b)
            batch=true
            shift
            ;;
        -h)
            shift
            ;;
        -i)
            input="$2"
            shift 2
            ;;
        -m)
            mode="$2"
            shift 2
            ;;
        -o)
            output="$2"
            output="${output%.*}"
            shift 2
            ;;
        *)
            echo "Invalid option: $key"
            exit 1
            ;;
    esac
done

# Check the options and execute corresponding functions
if [ "$batch" = true ]; then
    if [ -d "./batch" ]; then
        echo "${magenta}Running in batch mode."
        echo "${green}Select a mode:"
        select op_mode in "hex" "son" "gif" "mosh"; do
            case $op_mode in
                "hex")
                    ask_cleanup
                    echo "Cleanup option selected: $clean"
                    hex_mode
                    break
                    ;;
                "son")
                    ask_cleanup
                    echo "Cleanup option selected: $clean"
                    son_mode
                    break
                    ;;
                "gif")
                    ask_cleanup
                    echo "Cleanup option selected: $clean"
                    gif_mode
                    break
                    ;;
                "mosh")
                    mosh_mode
                    break
                    ;;

                *)
                    echo "Invalid option. Please try again."
                    ;;
            esac
        done
    else
        echo "${red}Directory 'batch' does not exist in the current working directory. Please create the directory and fill it with source material."
        exit 1
    fi
else
    if { [ -z "$input" ] || [ -z "$mode" ]; } && [ "$mode" != "mosh" ]; then
        echo "${green}Invalid / empty option(s). Please provide all the required options."
        echo "${green}Here's the help file:"
        mgt_help
        exit 1
    fi

    case $mode in
        "hex")
            ask_cleanup
            echo "Cleanup option selected: $clean"
            hex_mode
            ;;
        "son")
            ask_cleanup
            echo "Cleanup option selected: $clean"
            son_mode
            ;;
        "gif")
            ask_cleanup
            echo "Cleanup option selected: $clean"
            gif_mode
            ;;
        "mosh")
            mosh_mode
            ;;
        "test")
            test_mode
            ;;
        *)
            echo "${red}Invalid mode: $mode. Please try again."
            exit 1
            ;;
    esac
fi

exit 0
